class TestIPCA
{
    public static void main(String args[])
    {
        if(args.length == 3)
        {
            double Tin = Double.parseDouble(args[0]);
            int d = Integer.parseInt(args[1]);
            String filename = args[2];
            
            IPCA obj = new IPCA(Tin, d, filename);
        }
	else
		System.out.println("USAGE: TestIPCA <Tin Value> <SPD Value> <Filename>");
    }
}
