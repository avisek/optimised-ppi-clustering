//CODING DONE - DEBUGGING DONE - TO BE CHECKED
import java.util.ArrayList;
class Vertex
{
    int index;
    double weight;
    double INvk;
    ArrayList <Integer> neighbours;
    boolean seen;
    public Vertex(int ind)
    {
        index = ind;
        weight = 0;
        INvk = 0;
        neighbours = new ArrayList<Integer>();
        seen = false;
    }
    public void setIndex(int i)
    {
        index = i;
    }
}
class Cluster
{
    ArrayList<Vertex> vertices;
    int SP;
    public Cluster()
    {   
        vertices = new ArrayList<Vertex>();
        SP=0;
    }
}
class IPCA
{
    Graph g;
    Vertex vertices[];
    ArrayList<Cluster> clusters;
    //Input parameters
    double Tin;
    int d;
    
    public IPCA(double T, int spd, String filename)
    {
    	g = new Graph(filename);   //GET THE GRAPH
	System.out.println("NUMBER OF VERTICES:\t"+g.order);
        vertices = new Vertex[g.order];
        clusters = new ArrayList<Cluster>();
        for(int i=0; i<g.order; i++)
        {
            vertices[i] = new Vertex(i);
        }
        Tin = T;
        d = spd;
        
        g.printMatrix();
        
        formClusters();
        //System.out.print(vertices[10].weight);
    }
    public void formClusters()
    {
	int clusterCount=1;
        //Step 1
        vertexWeight(); 
        qsortByWeight(vertices, 0, vertices.length-1);
        //Step 2
        for(int i=0; i<vertices.length; i++)
        {
            Cluster cl = new Cluster();
            if(vertices[i].seen == false)
            {
                cl.vertices.add(vertices[i]);
                clusters.add(cl);
                vertices[i].seen = true;
                extendCluster(cl, vertices[i]);
//		for(int j=0; j<vertices.length; j++)
//		{
//			System.out.print(vertices[i].seen+"  ");
//		}
//		System.out.println();
                printCluster(cl, clusterCount++);
            }
    	}
    }   
    public boolean addUnseenNeighbours(Vertex v, ArrayList<Vertex> N)
    {
    	boolean flag = false;
    	for(int i=0; i<g.order; i++)
    	{
    		if(vertices[i].seen == true)	continue;
    		if(g.im[v.index][vertices[i].index] == true)
    		{
    			vertices[i].seen = true;
    			N.add(vertices[i]);
    			flag = true;
    		}
    	}
    	return flag;
    }
    public int getSP(Cluster cl, Vertex v)
    {
    	//Implement Dijkstra Algorithm for shortest path from v to all vertices
    	int pathWeight[] = new int[cl.vertices.size()];
    	int colour[] = new int[cl.vertices.size()];
    	for(int i=0; i<cl.vertices.size(); i++)
    	{
    		pathWeight[i] = cl.vertices.size() + 1;	
    		colour[i] = 0;					//Initializing path weights to infinity and colour to 0
    	}
    	Vertex pivot = v;
    	int selfWeight = 0;
    	while(true)
    	{
    		for(int i=0; i<cl.vertices.size(); i++)	//Allocating pathweights to neighbours
    		{
                //System.out.println(i +" Index 1 ||" + cl.vertices.get(i).index + "Index 2 ||"+ pivot.index);
                //System.out.println(colour.length +" || Match with ||" + g.order);
    			if(colour[i]==0 && g.im[cl.vertices.get(i).index][pivot.index] == true)	//checking for edge compatible for relaxation
    			{
                    //System.out.println(pathWeight[0]);
    				if(pathWeight[i] > selfWeight + 1)	//checking for edge relaxation
    				{
    					pathWeight[i] = selfWeight + 1;
    				}
    			}
    		}
			int small = -1;
    		for(int i=0; i<cl.vertices.size(); i++)
    		{
    			if(small==-1 && colour[i]==0 || colour[i]==0 && pathWeight[i]<pathWeight[small])
    			{
    				small = i;
    			}
    		}
    		if(small==-1)
    			break;
    		else
    		{
    			pivot = cl.vertices.get(small);
    			selfWeight = pathWeight[small];
                colour[small] = 1;
    		}
    	}
        //System.out.println("DONE FOR ONCE");							
    	//Find longest of these paths
    	int maxSP = 0;
    	for(int i=0; i<cl.vertices.size(); i++)
    	{
    		if(maxSP < pathWeight[i])	maxSP = pathWeight[i];
    	}
		
		//If new path length > Cluster SP then Cluster SP = new path length
		if(maxSP>cl.SP)		cl.SP = maxSP;
		
		//Return cluster SP
		return cl.SP;
    }
    public void extendCluster(Cluster cl, Vertex seed)
    {
    	ArrayList<Vertex> N = new ArrayList<Vertex>();
    	ArrayList<Vertex> Temp = new ArrayList<Vertex>();
    	do
    	{
//System.out.println("Seed: "+seed.index);
    		addUnseenNeighbours(seed, N);
    		if(N.size() == 0)
    		{
    			break;
    		}
	    	for(int i=0; i<N.size(); i++)
	    	{
   		 		calculateINvk(cl, N.get(i));
    		}
    		Vertex bestNeighbour=N.get(0);
    		Vertex big = N.get(0);
    		for(int i=1; i<N.size(); i++)
    		{
				if (N.get(i).INvk > bestNeighbour.INvk)
					bestNeighbour = N.get(i);
    		}
    		if(bestNeighbour.INvk >= Tin)
    		{ 		
//System.out.println("BEST NEIGHBOUR and its INVK IS: "+bestNeighbour.index+"  " + bestNeighbour.INvk);
    			
    			if(getSP(cl, bestNeighbour)<d)	//neighbour accepted - look for new
    			{
    				cl.vertices.add(bestNeighbour);
    				seed = bestNeighbour;
				/*for(int i=0; i<Temp.size(); i++)
				{
					Temp.get(i).seen = false;
					Temp.remove(i);
System.out.println("TEMP REMOVAL: "+g.vertices.get(Temp.get(i).index));
System.out.println("TEMP STATUS:");	for(int j=0; j<Temp.size(); j++) System.out.print(g.vertices.get(Temp.get(j).index)+"  "); System.out.println();	
				}*/
				while(Temp.size()>0)
				{
					Temp.get(0).seen = false;
					Temp.remove(0);
				}
//System.out.println("NEIGHBOUR ACCEPTED");
                	}
    			else	//Problem with d value - go to 3.1
    			{
    				//bestNeighbour.seen = false;
				Temp.add(bestNeighbour);
//System.out.println("TEMP ADDITION: "+g.vertices.get(bestNeighbour.index));
//System.out.println("TEMP STATUS:");	for(int j=0; j<Temp.size(); j++) System.out.print(g.vertices.get(Temp.get(j).index)+"  "); System.out.println();	
//System.out.println("NEIGHBOUR REJECTED DUE TO D VALUE");
    			}
    			N.remove(bestNeighbour);
//System.out.println("NEIGHBOUR REMOVED");
    		}
    		else	//Problem with Tin value - end cluster go to step 2
    		{
    			for(int i=0; i<N.size(); i++)
    			{
    				N.get(i).seen = false;
    			}
			/*for(int i=0; i<Temp.size(); i++)
			{
				Temp.get(i).seen = false;
				Temp.remove(i);
System.out.println("TEMP REMOVAL: "+ g.vertices.get(Temp.get(i).index));
System.out.println("TEMP STATUS:");	for(int j=0; j<Temp.size(); j++) System.out.print(g.vertices.get(Temp.get(j).index)+"  "); System.out.println();	
			}*/	
			while(Temp.size()>0)
			{
				Temp.get(0).seen = false;
				Temp.remove(0);
			}
//System.out.println("NEIGHBOUR REJECTED DUE TO Tin VALUE");
    			break;
    		}
		
	}while(N.size() >0);

	/*for(int i=0; i<Temp.size(); i++)
	{
		Temp.get(i).seen = false;
System.out.println("TEMP REMOVAL: "+ g.vertices.get(Temp.get(i).index));
System.out.println("TEMP STATUS:");	for(int j=0; j<Temp.size(); j++) System.out.print(g.vertices.get(Temp.get(j).index)+"  "); System.out.println();	
		Temp.remove(i);
	}*/
	while(Temp.size()>0)
	{
		Temp.get(0).seen = false;
		Temp.remove(0);
	}
	
    }            
    public void vertexWeight()
    {
        for(int i=0; i<g.order; i++)
        {
            for(int j=i+1; j<g.order; j++)
            {
                if(g.im[i][j] == true)
                {
                    int edgeWeight = 0;
                    for(int k=0; k<g.order; k++)
                    {
                        if(j==k || i==k)    continue;
                        if(g.im[i][k]==true && g.im[j][k]==true)
                        {
                            edgeWeight++;
                        }
                    }
                    vertices[j].weight += edgeWeight;
                    vertices[i].weight += edgeWeight;
                }
            }
        }
    }
    public static void qsortByWeight(Vertex avertex[], int i, int j)
    {
        if(j > i && i < j)
        {
            Vertex vertex = avertex[i];
            int k = i + 1;
            for(int l = i + 1; l <= j; l++)
                if(vertex.weight < avertex[l].weight)
                {
                    Vertex vertex1 = avertex[l];
                    avertex[l] = avertex[k];
                    avertex[k] = vertex1;
                    k++;
                }

            avertex[i] = avertex[k - 1];
            avertex[k - 1] = vertex;
            qsortByWeight(avertex, i, k - 2);
            qsortByWeight(avertex, k, j);
        }
    }
    public double calculateINvk(Cluster c, Vertex v)
    {
        int connectivity=0;
        for(int i=0; i<c.vertices.size(); i++)
        {
            if(g.im[v.index][c.vertices.get(i).index] == true)
            {
                connectivity++;
            }
        }
        //System.out.println("CONNECTIVITY = " + connectivity +" | SIZE = "+ c.vertices.size());
        double result = 1.0 * connectivity/c.vertices.size();
		v.INvk = result;
        return result;
    }
    public void printCluster(Cluster cl, int count)
    {
        System.out.print(count+". CLUSTER FORMED\t");
        for(int i=0; i<cl.vertices.size(); i++)
        {
            System.out.print((String)g.vertices.get(cl.vertices.get(i).index) + " "); 
        }
        System.out.println();
    }
}
