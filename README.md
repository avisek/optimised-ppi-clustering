This project is an attempt to compare and optimise various clustering algorithms popular with Protein-Protein Interaction clusters. We have taken five most prominent clustering algorithms and tested their performance on free and open test data. The results of comparasion have been summarised in the accompanied document. 

Algorithms under study:
1. DPClus
2. IPCA
3. MCODE
4. LCMA
5. CFinder

The project includes 3 of the test data sets that had been used during test that had been collected from freely available databases.