import java.io.*;
import java.util.*;

public class Graph
{
	int order;
	Vector<String> vertices;
	int [][]im;
	Vector <Clique>cliques;
	int max_clique;
	public Graph(int ord)
	{
		cliques = new Vector<Clique>();
		vertices = new Vector<String>();
		order = ord;
		max_clique = 0;
		im = new int[order][order];
	}

	public Graph(String file)
	{
		try
		{
			cliques = new Vector<Clique>();
			BufferedReader r = new BufferedReader(new FileReader(file));
			vertices = new Vector<String>();
			while(true)
			{
				String temp = r.readLine();
				if(temp == null)	
					break;
				StringTokenizer inLine = new StringTokenizer(temp);
				String v1 = inLine.nextToken();
				String v2 = inLine.nextToken();
				
				int i;
				for(i=0;i<vertices.size();i++)
				{
					if(v1.equals(vertices.elementAt(i)))
						break;
				}
				if(i==vertices.size())
					vertices.addElement(v1);
			
				for(i=0;i<vertices.size();i++)
				{
					if(v2.equals(vertices.elementAt(i)))
						break;
				}
				if(i==vertices.size())
					vertices.addElement(v2);
			}
	
			im = new int[vertices.size()][vertices.size()];
			order = vertices.size();
			r.close();
			r = new BufferedReader(new FileReader(file));
			while(true)
			{
				String temp = r.readLine();
				if(temp==null)
					break;
				StringTokenizer inLine = new StringTokenizer(temp);
				String v1 = inLine.nextToken();
				String v2 = inLine.nextToken();
				int i,j;
				for(i=0; i<vertices.size(); i++)
				{
					if(v1.equals(vertices.elementAt(i)))
						break;
				}
				for(j=0; j<vertices.size(); j++)
				{
					if(v2.equals(vertices.elementAt(j)))
						break;
				}
				im[i][j] = im[j][i] = 1;
			}
			r.close();
		}
		catch(IOException e)
		{
			System.out.println("Error");
		}
	}	

	public void printMatrix()
	{
		System.out.println("VERTICES:");
		for(int i=0; i<vertices.size(); i++)
			System.out.print(vertices.elementAt(i) + "\t");
		System.out.println("\nEDGES:");
		for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				System.out.print(im[i][j]+"\t");
			}
			System.out.println();
		}
	}

	public void generate_cliques()
	{
		Vector<Integer> P = new Vector<Integer>();
		for(int i=0;i<order;i++)
		{
			P.addElement(new Integer(i));
		}
		Vector<Integer> X = new Vector<Integer>();
		Vector<Integer> R = new Vector<Integer>();
		bronKerbosch(R,P,X);
	}
	
	private void bronKerbosch(Vector<Integer> R, Vector<Integer> P, Vector<Integer> X)
	{
		if(P.size() == 0 && X.size() == 0)
		{
			Clique c = new Clique(R);
//System.out.println(R.toString() + "\t Order: " + R.size());
			cliques.addElement(c);
			if(R.size() > max_clique)		max_clique = R.size();	
			return;
		}
		else
		{
			int i=0;
			while(i<P.size())
			{
				int v = P.elementAt(i).intValue();
				Vector<Integer> newR = (Vector<Integer>)R.clone();
				Vector<Integer> newP;
				Vector<Integer> newX; 
				
				newR.addElement(new Integer(v));
				newP = Graph.intersection(P, findNeighbors(v));
				newX = Graph.intersection(X, findNeighbors(v));
				bronKerbosch(newR, newP, newX);

				P.removeElementAt(i);
				X.addElement(new Integer(v));
			}	
		}
	}

	
	private Vector<Integer> findNeighbors(int a)
	{
		Vector<Integer> res = new Vector<Integer> ();
		for(int i=0; i<order; i++)
		{
			if(im[a][i] == 1)
			{
				res.addElement(i);
			}
		}
		return res;
	} 
	public static Vector<Integer> intersection (Vector<Integer> a, Vector<Integer> b)
	{
		Vector<Integer> res = new Vector<Integer>();
		
		for(int i=0; i<a.size(); i++)
		{	
			int v = a.elementAt(i);
			for(int j=0; j<b.size(); j++)
			{
				if(v == b.elementAt(j))
				{
					res.addElement(new Integer(v));
					break;
				}
			}
		}
		return res;
	}
	private boolean is_complete(int list[], int n)
	{
		for(int i=0; i<n; i++)
		{
			for(int j=i+1; j<n; j++)
			{
				if(im[list[i]][list[j]] == 0)
					return false;
			}
		}
		return true;
	}
}



