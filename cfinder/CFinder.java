import java.util.*;
class Cluster
{
	Vector<Clique> cliques;
	int kValue;
	public Cluster(int kval)
	{
		kValue = kval;
		cliques = new Vector<Clique> ();
	}
}
class CFinder
{
	Vector <Object>sc;
	int [][] overlap;
	private Graph g;
	//int clusters[];	//might not be required after change
	Vector<Cluster> clusters;
	
	public CFinder(String file)
	{
		sc = new Vector<Object>();
		g = new Graph(file);
		clusters = new Vector<Cluster>();
		System.out.println("GENERATING CLIQUES");
		g.generate_cliques();
		System.out.println("CLIQUES: ");
		for(int i=0; i<g.cliques.size(); i++)
		{	
			System.out.print("["+g.cliques.elementAt(i).vertex.toString() +"]  ");
		}
		
		generateOverlap();
		int i=0;
//System.out.println("VALUE OF K RANGES FROM 2 TO " + g.max_clique);
		for(i=2; i<=g.max_clique; i++)
		{
//System.out.println("CLUSTERING WITH K = " + i ); 
			cluster(i);
		}
		printClusters();
	}
	private void printClusters()
	{
		int counter=0;
		for(int i=0; i<clusters.size(); i++)
		{
			Cluster cl = clusters.elementAt(i);
			if(i==0 || cl.kValue != clusters.elementAt(i-1).kValue)
				counter=0;
			System.out.print("CLUSTER "+(counter++)+":\t(K="+cl.kValue+")\t");
			for(int j=0; j<cl.cliques.size(); j++)
			{
				Clique clq = cl.cliques.elementAt(j);
				System.out.print("(");
				for(int k=0; k<clq.vertex.size(); k++)
				{
					System.out.print(g.vertices.elementAt(clq.vertex.elementAt(k))+" ");
				}
				System.out.print("\b) ");
			}
			System.out.println();
		}
	}
	public void generateOverlap()
	{
		int n = g.cliques.size();
		overlap = new int [n][n];
		for(int i=0; i<n; i++)
		{
			Clique c1 = (Clique)g.cliques.get(i);
			for(int j=0; j<n; j++)
			{
				Clique c2 = (Clique)g.cliques.get(j);
				overlap[i][j] = Graph.intersection(c1.vertex, c2.vertex).size();
			}
		}
		System.out.println("Overlap Matrix Generated");
	}
		
	private void cluster(int k)
	{
		Graph tg = new Graph(g.cliques.size());
		int i=0;
		while(i<g.cliques.size())
		{
			Clique c = g.cliques.elementAt(i);
			if(c.vertex.size()<k)
			{
				int j;
				for(j=0;j<g.cliques.size();j++)
				{
					tg.im[i][j] = -1;
					tg.im[j][i] = -1;
//System.out.println("\t\tLooping 1st for loop with j = "+j);
				}
				i++;
				continue;
			}
			int j;
			for(j=0;j<g.cliques.size();j++)
			{
				if(overlap[i][j] >= k)
				{
					if(overlap[i][j] == -1)
						break;
					else
						tg.im[i][j]= 1;
				}
//System.out.println("\t\tLooping 2nd for loop with j = "+j);
			}	
			i++;
//System.out.println("\tLooping While Loop with i = " + i);
		}
		//tg.printMatrix();
		dfsMakeCluster(tg, k);
	}
	private void dfsMakeCluster(Graph tg, int kval)
	{
		boolean seen[] = new boolean[tg.order];
		for(int i=0; i<tg.order; i++)
			seen[i] = false;		//WHITE
		for(int i=0; i<tg.order; i++)
		{
			if(tg.im[i][0] == -1 || seen[i] == true)	
				continue;
			Cluster cl = new Cluster(kval);
			dfs(i, tg, seen, cl);
			clusters.addElement(cl);
		}
	}
	
	private void dfs(int seed, Graph tg, boolean seen[], Cluster cl)
	{
		cl.cliques.addElement(g.cliques.elementAt(seed));	//Add seed to the cluster
		seen[seed] = true;					//Seed is seen
		for(int i=0; i<tg.order; i++)
		{
			if(tg.im[seed][i] == 1 && seen[i] == false)
			{
				dfs(i, tg, seen, cl);
			}
		}
	}
}




