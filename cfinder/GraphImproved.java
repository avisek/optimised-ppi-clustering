import java.io.*;
import java.util.*;

public class Graph
{
	int order;
	int [][]im;
	Vector <String>vertices;
	Vector <Clique>cliques;
	int max_clique;
	public Graph(int o)
	{
		cliques = new Vector<Clique>();
		order = o;
		max_clique = 0;
		im = new int[order][order];
	}
	public Graph(String f)
	{
		try
		{
			cliques = new Vector<Clique>();
			BufferedReader r = new BufferedReader(new FileReader(f));
			order = Integer.parseInt(r.readLine());
			im = new int[order][order];
			while(true)
			{
				String line = r.readLine();
				if(line.equalsIgnoreCase("END"))
				{
					break;
				}
				int space_pos = line.indexOf(' ');
				int v1 = Integer.parseInt(line.substring(0, space_pos)) - 1;
				int v2 = Integer.parseInt(line.substring(space_pos+1)) - 1;
				im[v1][v2] = 1;
				im[v2][v1] = 1;
			}
		}
		catch(IOException e)
		{
			System.out.println("INPUT OUTPUT EXCEPTION");
		}		
	}
	public void printMatrix()
	{
		for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				System.out.print(im[i][j]+" ");
			}
			System.out.println();
		}
	}
	public void generate_cliques()
	{
		Vector<Integer> P = new Vector<Integer>();
		for(int i=0;i<order;i++)
		{
			P.addElement(new Integer(i+1));
		}
		Vector<Integer> X = new Vector<Integer>();
		Vector<Integer> R = new Vector<Integer>();
		bronKerbosch(R,P,X);
	}
	
	private void bronKerbosch(Vector<Integer> R, Vector<Integer> P, Vector<Integer> X)
	{
		if(P.size() == 0 && X.size() == 0)
		{
			Clique c = new Clique(R);
			System.out.println(R.toString() + "\t Order: " + R.size());
			cliques.addElement(c);
			if(R.size() > max_clique)		max_clique = R.size();	
			return;
		}
		else
		{
			int i=0;
			while(i<P.size())
			{
				int v = P.elementAt(i).intValue();
				Vector<Integer> newR = (Vector<Integer>)R.clone();
				Vector<Integer> newP;
				Vector<Integer> newX; 
				
				newR.addElement(new Integer(v));
				newP = Graph.intersection(P, findNeighbors(v));
				newX = Graph.intersection(X, findNeighbors(v));
				bronKerbosch(newR, newP, newX);

				P.removeElementAt(i);
				X.addElement(new Integer(v));
			}	
		}
	}

	
	private Vector<Integer> findNeighbors(int a)
	{
		Vector<Integer> res = new Vector<Integer> ();
		for(int i=0; i<order; i++)
		{
			if(im[a-1][i] == 1)
			{
				res.addElement(i+1);
			}
		}
		return res;
	} 
	public static Vector<Integer> intersection (Vector<Integer> a, Vector<Integer> b)
	{
		Vector<Integer> res = new Vector<Integer>();
		
		for(int i=0; i<a.size(); i++)
		{	
			int v = a.elementAt(i);
			for(int j=0; j<b.size(); j++)
			{
				if(v == b.elementAt(j))
				{
					res.addElement(new Integer(v));
					break;
				}
			}
		}
		return res;
	}
	private boolean is_complete(int list[], int n)
	{
		for(int i=0; i<n; i++)
		{
			for(int j=0; j<n; j++)
			{
				if(im[list[i]][list[j]] == 0)
					return false;
			}
		}
		return true;
	}
}



