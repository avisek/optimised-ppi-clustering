import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;

public class Graph
{
    	int order;
    	ArrayList<String> vertices;
    	boolean im[][];
	int weight[][];
	int vertex_weight[];
	int vertex_degree[];

    	public Graph(int i)
    	{
        	order = i;
        	vertices = new ArrayList<String>();
        	im = new boolean[order][order];
		weight = new int[order][order];
		vertex_weight = new int[order];
		vertex_degree = new int[order];
   	}
    	public Graph(String file)
    	{
        	try
        	{
            		BufferedReader r = new BufferedReader(new FileReader(file));
            		vertices = new ArrayList<String>();
            		do
            		{
                		String s1 = r.readLine();
                		if(s1 == null)
                    			break;
                		StringTokenizer stringtokenizer = new StringTokenizer(s1);
                		String s3 = stringtokenizer.nextToken();
                		String s5 = stringtokenizer.nextToken();
                		int i;
                		for(i = 0; i < vertices.size() && !s3.equals(vertices.get(i)); i++);
               			if(i == vertices.size())
                  			vertices.add(s3);
                		for(i = 0; i < vertices.size() && !s5.equals(vertices.get(i)); i++);
                		if(i == vertices.size())
                   			vertices.add(s5);
           	 	} while(true);
            		r.close();
            		im = new boolean[vertices.size()][vertices.size()];
            		order = vertices.size();
			weight = new int[order][order];
			vertex_weight = new int[order];
			vertex_degree = new int[order];
            		r = new BufferedReader(new FileReader(file));
            		do
            		{
                		String s2 = r.readLine();
                		if(s2 == null)
                    		break;
                		StringTokenizer stringtokenizer1 = new StringTokenizer(s2);
                		String s4 = stringtokenizer1.nextToken();
                		String s6 = stringtokenizer1.nextToken();
                		int j;
                		for(j = 0; j < vertices.size() && !s4.equals(vertices.get(j)); j++);
                		int k;
                		for(k = 0; k < vertices.size() && !s6.equals(vertices.get(k)); k++);
                		im[j][k] = im[k][j] = true;
            		} while(true);
            		r.close();
        	}
        	catch(IOException ioexception)
        	{
            		System.out.println("Error");
        	}
    	}

	public void printMatrix()
    	{
        	System.out.println("VERTICES:");
        	for(int i = 0; i < vertices.size(); i++)
            		System.out.print(vertices.get(i)+" ");
		System.out.println();
/*
        	System.out.println("-------------------------\nEDGES:");
        	for(int j = 0; j < order; j++)
        	{
            		for(int k = 0; k < order; k++)
                		System.out.print((new StringBuilder()).append(im[j][k]).append(" ").toString());

            		System.out.println();
        	}
*/
    	}

    	public ArrayList findNeighbours(int i)
    	{
        	ArrayList<Integer> nbr = new ArrayList<Integer>();
        	for(int j = 0; j < order; j++)
	            if(im[i][j] == true)
                	//nbr.add(Integer.valueOf(j));
                	nbr.add(j);

        	return nbr;
    	}
	public boolean vertex_degree_calc()
	{
	/*	for(int i=0;i<order;i++)
		{
			vertex_degree[i]=0;
		}*/
		
		for(int i=0;i<order;i++)
		{
			
			for(int j=0;j<order;j++)
			{
				if(im[i][j]) vertex_degree[i]++;
			}
		}

             	for(int i=0;i<order;i++)
		{
			if(vertex_degree[i] > 0)
			     return true;
	        }

		return false;
	}
  	public void vertex_weight_calc()
  	{
		/*for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
	         	{
				weight[i][j]=0;
        		}	
		}
  
                for(int i=0;i<order;i++)
		{
			vertex_weight[i]=0;
		}
*/	
		for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				for(int k=0;k<order;k++)
				{
					if(im[i][k] && im[k][j] && im[i][j] && i!=j)
				    	{
						weight[i][j]++;
				    	}
				}	
			}
		}
                for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				vertex_weight[i]+=weight[i][j];
			}
		}
	}
}
