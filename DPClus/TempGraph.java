import java.io.*;
import java.util.*;


public class Graph
{
	int order;
	int adj_mat[][];
	int weight[][];
	int vertex_weight[];
        int vertex_degree[];
	
	public Graph(int or)
	{
		order=or;
		adj_mat=new int[order][order];
		weight=new int[order][order];
		vertex_weight=new int[order];
		vertex_degree=new int[order];
		
	}

	public Graph(String f)
	{
		try
		{
			BufferedReader in=new BufferedReader(new FileReader(f));
			order=Integer.parseInt(in.readLine());
			adj_mat=new int[order][order];
                        weight=new int[order][order];
			vertex_weight=new int[order];
					
			while(true)
			{
				String line=in.readLine();
				if(line.equalsIgnoreCase("END"))
				{
					break;
				}
				
				int spacepos=line.indexOf(' ');
				int v1=Integer.parseInt(line.substring(0,spacepos))-1;
				int v2=Integer.parseInt(line.substring(spacepos+1))-1;
				adj_mat[v1][v2]=1;
				adj_mat[v2][v1]=1;
				
                	 }
                }catch(Exception e)
		{
			System.out.println("Prob.!!!");
		}
		
        }

      	public void print_adj_matrix()
	{
		
		for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				System.out.print(adj_mat[i][j]+" ");
			}
			
			System.out.println();
		}
	}

      	public boolean vertex_degree_calc()
	{
		for(int i=0;i<order;i++)
		{
			vertex_degree[i]=0;
		}
		
		for(int i=0;i<order;i++)
		{
			
			for(int j=0;j<order;j++)
				{
					vertex_degree[i]+= adj_mat[i][j];
				}
		}

              	for(int i=0;i<order;i++)
		{
			if(vertex_degree[i] > 0)
			     return true;
	        }

		return false;
	}

  	public void vertex_weight_calc()
  	{
		for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
	         	{
				weight[i][j]=0;
        		}	
		}
  
                for(int i=0;i<order;i++)
		{
			vertex_weight[i]=0;
		}
	
		for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				for(int k=0;k<order;k++)
				{
					if(adj_mat[i][k]==1 && adj_mat[j][k]==1 && adj_mat[i][j]==1 && i!=j)
				    	{
						weight[i][j]++;
				    	}
				}	
			}
		}
                for(int i=0;i<order;i++)
		{
			for(int j=0;j<order;j++)
			{
				vertex_weight[i]+=weight[i][j];
			}
		}
	}
   //Finding the highest weight vertex here 
/*	public int get_max_vertex()
	{
                int max_weight= vertex_weight[0];
	       	int max_weight_vertex=0;                        
               	for(int i=0;i<order;i++)
		{
			if(max_weight < vertex_weight[i])
			{
				max_weight=vertex_weight[i];
				max_weight_vertex= i;
                        }
                }
     		return max_weight_vertex;
	}
*/
}

