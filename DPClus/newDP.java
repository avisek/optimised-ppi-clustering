import java.util.*;
import java.io.*;
class Vertex
{
	int index;
	int curWeight;
	int connectivity;
	int weight;
	int degree;
	boolean seen;
	public Vertex()
	{
	}
	public Vertex(int i, Graph g)
	{
		index = i;
		curWeight = connectivity = 0;
		weight = g.vertex_weight[i];
		degree = g.vertex_degree[i];
		seen = false;
	}
} 
class DPclus
{
   	Graph g;
   	Vector<Integer> cluster;//###how to store cluster list in a single integer vector
   	Vertex vertices[];
	double Dpin;
   	double Cpin;
   	int k;

   	public DPclus(String file,double init_dpin,double init_cpin)
   	{
		g= new Graph(file);
		vertices = new Vertex[g.order];
                for(int i=0; i<g.order; i++)
			vertices[i] = new Vertex(i, g);
		cluster=new Vector<Integer>(); 
		Dpin=init_dpin;
		Cpin=init_cpin;
		k=1;
   	}
	
	public void formClusters()
	{
		int seedVertex;
	 	if(!g.vertex_degree_calc())
		{
			return;
		}
		g.vertex_weight_calc();
		//get the seed vertex
		int seed = 0; 	
		for(int i=0; i<g.order; i++)
		{
			seed = g.vertex_weight[seed] < g.vertex_weight[i]?i:seed;
		}
		if(g.vertex_weight[seed]==0)
		{
			seed = 0;
			for(int i=0; i<g.order; i++)
			{
				seed = g.vertex_degree[seed] < g.vertex_degree[i]?i:seed;
			}
		}
		//Seed vertex accertained
		Cluster cluster = new Cluster();
		cluster.vertices.addElement(vertices[seed]);
		extendCluster(cluster, seed);
	}

	public void extendCluster(Cluster cl, int seed)
	{
		int v = seed;
		boolean genNeighbours = true;
		Vector<Vertex> N = new Vector<Vertex>();
		while(true)
		{
			//Generate neighbours for vertex v in cluster
			if(genNeighbours)
			{
				for(int i=0; i<g.order; i++)
				{
					if(g.adj_mat[v][i] == 1)
					{
						if(vertices[i].seen == false)
						{
							N.addElement(vertices[i]);
							vertices[i].curWeight = g.weight[v][i];
							vertices[i].connectivity = 1;
							vertices[i].seen = true;
						}
						else
						{
							vertices[i].curWeight += g.weight[v][i];
							vertices[i].connectivity ++;
						}
					}
				}
				genNeighbours = false;
			}
			//Generated
			
			//Check if neighbour array is empty
			if(N.size() == 0)	break;
			//Checked for emptiness
			
			//Finetune Neighbours - CANT DO YET
		
			//Get the best neighbour - means neighbour with max weight then max connectivity (double sorting)
			Vertex best=N.elementAt(0);
			for(int i=1; i<N.size(); i++)
			{
				Vertex test = N.elementAt(i);
				if(test.curWeight > best.curWeight)
					best = test;
				else if(test.curWeight == best.curWeight && test.connectivity > best.connectivity)
					best = test;
			}
			//Found the best neighbour and kept it in best.
			
			//Add best neighbour to cluster and remove from neighbour array
			cl.vertices.addElement(best);
			best.connectivity = 0;
			best.curWeight = 0;
			N.removeElement(best);
			//Added and removed from neighbour array	
			
			//Check CPnk and Dk values
			double testCPNK = calcCPNK(cl);
			int testDK = calcDK(cl);
			if(testCPNK < Cpin || testdk < Dpin)	//CASE OF MISMATCH
			{
				cl.vertices.removeElement(best);
				best.seen = false;
			}
			else
			{
				v = best.index;
				genNeighbours = true;
			}
			//Checked
		}//Repeat till there exists neighbours in N
	}
}	
