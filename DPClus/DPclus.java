import java.util.*;
import java.io.*;
class Vertex
{
	int index;
	int curWeight;
	int connectivity;
	int weight;
	int degree;
	boolean seen;
	boolean clustered;
/*	public Vertex()
	{
	}
*/	public Vertex(int i, Graph g)
	{
		index = i;
		curWeight = connectivity = 0;
		weight = g.vertex_weight[i];
		degree = g.vertex_degree[i];
		seen = false;
		clustered = false;
	}
} 
class DPclus
{
   	Graph g;
   	ArrayList<Cluster> clusters;//###how to store cluster list in a single integer vector
   	Vertex vertices[];
	double Dpin;
   	double Cpin;

   	public DPclus(String file,double init_dpin,double init_cpin)
   	{
		g= new Graph(file);
		clusters = new ArrayList<Cluster>();
		vertices = new Vertex[g.order];
        	g.vertex_degree_calc();
		g.vertex_weight_calc();	
		for(int i=0; i<g.order; i++)
			vertices[i] = new Vertex(i, g);
		//cluster=new ArrayList<Integer>(); 
		Dpin=init_dpin;
		Cpin=init_cpin;
		formClusters();
		printClusters();
   	}
	
	public void formClusters()
	{
		int seedVertex;
	 	if(!g.vertex_degree_calc())
		{
			return;
		}
		g.vertex_weight_calc();
		while(true)
		{
			//get the seed vertex
			int seed = 0; 	
			for(int i=0; i<g.order; i++)
			{
				if(vertices[i].clustered)
					continue;
				seed = (g.vertex_weight[seed]<g.vertex_weight[i] || vertices[seed].clustered)?i:seed;
			}
			if(g.vertex_weight[seed]==0)
			{
				seed = 0;
				for(int i=0; i<g.order; i++)
				{
					if(vertices[i].clustered)
						continue;
					seed = (g.vertex_degree[seed]<g.vertex_degree[i] || vertices[seed].clustered==true)?i:seed;
				}
			}
			//Seed vertex accertained
			if(vertices[seed].clustered==true)
			{
				System.out.println("Clustering Completed");
				break;
			}
			Cluster cluster = new Cluster();
			cluster.vertices.add(vertices[seed]);
			//vertices[seed].clustered = true;
//System.out.println("Got a new seed. Seed is "+seed);
			extendCluster(cluster, seed);
			clusters.add(cluster);
			for(int i=0; i<cluster.vertices.size(); i++)
				cluster.vertices.get(i).clustered=true;
//System.out.println("ADDED THIS CLUSTER");
//for(int i=0; i<cluster.vertices.size(); i++)
//	System.out.print(cluster.vertices.get(i).index+" ");
//System.out.println();
		}
	}
	
	public void printClusters()
	{
		System.out.println("CLUSTERING COMPLETED:\t");
		for(int i=0; i<clusters.size(); i++)
		{
			Cluster cl = clusters.get(i);
			System.out.print(i+". CLUSTER VERTICES:\t");
			for(int j=0; j<cl.vertices.size(); j++)
			{
				System.out.print(g.vertices.get(cl.vertices.get(j).index)+" ");
			}
			System.out.println();
		}
	}		

	public void extendCluster(Cluster cl, int seed)
	{
		int v = seed;
		vertices[v].seen = true;
		boolean genNeighbours = true;
		ArrayList<Vertex> N = new ArrayList<Vertex>();
		while(true)
		{
			//Generate neighbours for vertex v in cluster
			if(genNeighbours)
			{
				for(int i=0; i<g.order; i++)
				{
					if(g.im[v][i] == true && vertices[v].clustered == false)
					{
						if(vertices[i].seen == false)
						{
							N.add(vertices[i]);
							vertices[i].curWeight = g.weight[v][i];
							vertices[i].connectivity = 1;
							vertices[i].seen = true;
						}
						else
						{
							vertices[i].curWeight += g.weight[v][i];
							vertices[i].connectivity ++;
						}
					}
				}
				genNeighbours = false;
			}
			//Generated
			
			//Check if neighbour array is empty
			if(N.size() == 0)	break;
			//Checked for emptiness
			
			//Finetune Neighbours - case jaundice
		
			//Get the best neighbour - means neighbour with max weight then max connectivity (double sorting problem resolved) :))
			Vertex best=N.get(0);
			for(int i=1; i<N.size(); i++)
			{
				Vertex test = N.get(i);
				if(test.curWeight > best.curWeight)
					best = test;
				else if(test.curWeight == best.curWeight && test.connectivity > best.connectivity)
					best = test;
			}
//System.out.println("Got a best neighbour which is " + best.index);
			//Found the best neighbour and kept it in best.
			
			//Add best neighbour to cluster and remove from neighbour array
			cl.vertices.add(best);
//System.out.println("Added "+best.index);
			best.connectivity = 0;
			best.curWeight = 0;
			N.remove(best);
			//Added and removed from neighbour array	
			
			//Check CPnk and Dk values
			double testCPNK = calcCPNK(cl,best);
			double testDK = calcDK(cl);
//System.out.println("CHECK:\t TestCPNK "+testCPNK+" | Cpin "+Cpin+" | TestDK "+testDK+" | Dpin "+Dpin);				
			if(testCPNK < Cpin || testDK < Dpin)	//case of condition mismatchH
			{
				cl.vertices.remove(best);   //Hence removing the vertex unmatching the conditions.
//System.out.println("Removed");
				best.seen = false;
			}
			else
			{
//System.out.println("Accepted");
				v = best.index;                //Accepting the vertex matching the conditions.
				//best.clustered = true;
				genNeighbours = true;
			}
			//Checked
		}//Repeat till there exists neighbours in N
	}


	
	public double calcDK(Cluster cl) // calculating density of cluster k
	{

	
//for(int i=0; i<cl.vertices.size(); i++)
//	System.out.print(cl.vertices.get(i).index+" ");
//System.out.println();

	   	 int no_of_edges=0;
		 for(int k=0;k<cl.vertices.size();k++)
	 	 {
	    		for(int i=k+1;i<cl.vertices.size();i++)
	     		{
            			if(g.im[cl.vertices.get(k).index][cl.vertices.get(i).index]) //symmetric matrix problem sorted...ENJOY!!
				{
					no_of_edges++;  		
				}
//else
//{
//System.out.println("the position value for ("+cl.vertices.get(k).index+","+cl.vertices.get(i).index+") is "+g.im[cl.vertices.get(k).index][i]);
//}
               	       	}
	 	 }
	
		//no_of_edges/=2; //no.of edges in the cluster ascertained
//System.out.println("CalcDK| Edges "+no_of_edges);
		double density= (2.0*no_of_edges)/(1.0 * cl.vertices.size() * (cl.vertices.size()-1));
//System.out.println("CalcDK| Density "+density);
		return density;
      	}
	
	public double calcCPNK(Cluster cl,Vertex best) //calculating cluster property of node n wrt cluster k
	{
	
		boolean present=true; //to check whether a node is part of the cluster or not
		int j=0,Enk=0;
		
		while(best.index!=cl.vertices.get(j++).index && j < cl.vertices.size()) // checks if the node is part of the cluster or no
		{
			present=false;
		}		
		if(present==false) // ascertained that the node is not part of the cluster
		{	
			for(int k=0;k< cl.vertices.size();k++) //finds out the number of edges between a node i and the cluster.
			{
				if(g.im[cl.vertices.get(k).index][best.index])
					Enk++;
			}
		}
		double cluster_property= Enk/( calcDK(cl) * cl.vertices.size());
                return cluster_property;
	}
		
}	
