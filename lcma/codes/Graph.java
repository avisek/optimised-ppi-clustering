import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;

public class Graph
{
    int order;
    ArrayList vertices;
    int im[][];

    public Graph(int i)
    {
        order = i;
        vertices = new ArrayList();
        im = new int[order][order];
    }

    public Graph(String file)
    {
        try
        {
            BufferedReader r = new BufferedReader(new FileReader(file));
            vertices = new ArrayList();
            do
            {
                String s1 = r.readLine();
                if(s1 == null)
                    break;
                StringTokenizer stringtokenizer = new StringTokenizer(s1);
                String s3 = stringtokenizer.nextToken();
                String s5 = stringtokenizer.nextToken();
                int i;
                for(i = 0; i < vertices.size() && !s3.equals(vertices.get(i)); i++);
                if(i == vertices.size())
                    vertices.add(s3);
                for(i = 0; i < vertices.size() && !s5.equals(vertices.get(i)); i++);
                if(i == vertices.size())
                    vertices.add(s5);
            } while(true);
            im = new int[vertices.size()][vertices.size()];
            order = vertices.size();
            r.close();
            r = new BufferedReader(new FileReader(file));
            do
            {
                String s2 = r.readLine();
                if(s2 == null)
                    break;
                StringTokenizer stringtokenizer1 = new StringTokenizer(s2);
                String s4 = stringtokenizer1.nextToken();
                String s6 = stringtokenizer1.nextToken();
                int j;
                for(j = 0; j < vertices.size() && !s4.equals(vertices.get(j)); j++);
                int k;
                for(k = 0; k < vertices.size() && !s6.equals(vertices.get(k)); k++);
                im[j][k] = im[k][j] = 1;
            } while(true);
            r.close();
        }
        catch(IOException ioexception)
        {
            System.out.println("Error");
        }
    }

    public void printMatrix()
    {
        System.out.println("VERTICES:");
        for(int i = 0; i < vertices.size(); i++)
            System.out.print((String)vertices.get(i)+" ");
	System.out.println();
/*
        System.out.println("-------------------------\nEDGES:");
        for(int j = 0; j < order; j++)
        {
            for(int k = 0; k < order; k++)
                System.out.print((new StringBuilder()).append(im[j][k]).append(" ").toString());

            System.out.println();
        }
*/
    }

    public ArrayList findNeighbours(int i)
    {
        ArrayList nbr = new ArrayList();
        for(int j = 0; j < order; j++)
            if(im[i][j] == 1)
                nbr.add(Integer.valueOf(j));

        return nbr;
    }
}
