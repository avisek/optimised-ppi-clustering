import java.io.*;
import java.util.*;
import java.lang.*;

class vertex
{
	int index;
	Vector<vertex> neighbour;
	Vector<Integer> degree;
	int density;
	boolean seen;
	vertex()
	{
		index=-1;
		neighbour=new Vector<vertex> ();	//changed here from String vector to vector of vertices
		degree=new Vector<Integer> ();
		density=0;
		seen = false;
	}
	public boolean calcNeighbours(Graph g, vertex []vertices)	//calculating neighbours of calling object
	{
		boolean exitFlag = false;
		for(int i=0; i<g.order; i++)
		{
			if(g.im[index][i] == 1 && vertices[i].seen == false)
			{
				neighbour.addElement(vertices[i]);
				vertices[i].seen = true;
				exitFlag = true;
			}
		}
		return exitFlag;
	}
			
}

class Lcma
{
	Graph g;
	vertex []vertices;
	Vector<vertex> LC;

	Lcma(String s)
	{
		g=new Graph(s);		//initialise the graph
		vertices=new vertex[g.order];
		LC=new Vector<vertex>();
	}

	public void printClusters()
	{
		for(int i=0; i<LC.size(); i++)
		{
			vertex v = LC.elementAt(i);
			System.out.print("CLUSTER: "+ g.vertices.get(v.index));
			for(int j=0; j<v.neighbour.size(); j++)
			{
				System.out.print(","+g.vertices.get(v.neighbour.elementAt(j).index));
			}
			System.out.println();
		}
	}

	public void createCluster()	//This is the root function operating on the input to create clusters
	{
		for(int i=0;i<g.order;i++)
		{
			vertex v=new vertex();		//creating new vertex v.ect
			vertices[i] = v;		//keeping the vertex v in the vertex array
			v.index = i;			//index of vertex v is stored
		}

		for(int i=0;i<g.order;i++)
		{
			vertex v = vertices[i];
			if(v.seen == true)
				continue;
			v.seen = true;
			boolean nbrstat = v.calcNeighbours(g, vertices); 		//creating adjacency list of vertex v  -- how is dis being done?? 
			if(nbrstat == false)
			{
				v.seen = false;
				continue;
			}
			//Getting degrees for neighbour with respect to neighbourhood of the seed vertex v
			int sum_degrees=0;	//sum of degrees of vertices in neighbourhood wrt neighbourhood - used to find number of edges in neighbourhood
			for(int j=0;j<v.neighbour.size();j++)
			{	
				int degree=0;	//specified the degree of vertex j wrt to the neighbourhood - initialised to 0
				for(int k=0; k<v.neighbour.size(); k++)	//calculating the degree of vertex j wrt neighbourhood
				{
					if(g.im[v.neighbour.elementAt(j).index][v.neighbour.elementAt(k).index]==1)
						degree++;
				}
			    	sum_degrees+=degree;	
				v.degree.addElement(new Integer(degree));	//storing the degree of jth neighbour wrt to this neighbourhood at jth position of degree vector
			}

			Sort.quickSort(v.degree,v.neighbour,0,v.degree.size()-1);

			//Calculate density of neighbourhood
			int edges = (sum_degrees+v.neighbour.size())/2;		//edges = sum of degree of vertices/2, adding neighbour.size since sum_degree does not contain degree of seed vertex
			v.density = edges/((v.neighbour.size()+1)*(v.neighbour.size()));	//Number of vertices = size of neighbour + 1(seed)
			//Density calculated		
		
			boolean stop=false;
			do	//repeat until stop=false |line 10
			{	
				//checking if deleting the 0th neighbour improves cluster density
				int new_density=(edges-v.degree.elementAt(0))/(v.neighbour.size())*(v.neighbour.size()-1);
				if(new_density>v.density)	//better to remove - lets remove
				{
					v.neighbour.elementAt(0).seen = false;
					v.neighbour.removeElementAt(0);	
					edges=edges-v.degree.elementAt(0);
					//we need to calculate the degree array afresh
					v.degree.clear();
					for(int j=0;j<v.neighbour.size();j++)
					{	
						int degree=0;
						for(int k=0;k<v.neighbour.size();k++)
						{
							if(g.im[v.neighbour.elementAt(j).index][v.neighbour.elementAt(k).index]==1)
								degree++;
						}
						//sum_degrees+=degree;	//why do this shit again?
						v.degree.addElement(new Integer(degree));
					}	
				}
				else
				{
					stop=true;	//since the neighbours are sorted, checking rest of neighbours are useless
				}
			}while(stop==false);
			//Cluster has been acertained

			if(v.neighbour.size()>1)	//checking if cluster size greater than 2. Only then it is a valid cluster|Lines:21-25
				LC.addElement(v);
			else
			{
				if(v.neighbour.size() == 1)
					v.neighbour.elementAt(0).seen=false;
				v.seen = false;
				v.neighbour.clear();
			}		
       		}
	}	
}
