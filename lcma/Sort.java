import java.util.*;
public class Sort 
{
    	public static void quickSort(Vector<Integer> a, Vector<vertex> b, int p, int r)
    	{
       	 	if(p<r)
        	{
            		int q=partition(a,b,p,r);
            		quickSort(a,b,p,q);
            		quickSort(a,b,q+1,r);
        	}
    	}

    	private static int partition(Vector<Integer> a, Vector<vertex> b, int p, int r) 
	{
        	int x = a.elementAt(p);
        	int i = p-1 ;
	        int j = r+1 ;

       		while (true) 
		{
    	        	i++;
       			while ( i<r && a.elementAt(i) < x)
                		i++;
            		j--;
            		while (j>p && a.elementAt(j) > x)
                		j--;

            		if (i < j)
                		swap(a, b, i, j);
            		else
                		return j;
        	}
    	}

    	private static void swap(Vector<Integer> a, Vector<vertex> b, int i, int j) 
	{
		Collections.swap(a, i, j);
		Collections.swap(b, i, j);
    	}
}
