import java.io.*;
import java.util.ArrayList;
class Vertex
{
    double weight;
    double coreDensity;
    int coreNumber;
    int degree;
    int index;
    boolean seen;
    int haircutFlag;
    ArrayList<Integer> neighbors;
    
    public Vertex()
    {
        index = -1;
        degree = 0;
        coreNumber = 0;
        weight = coreDensity = 0.0D;
        seen = false;
        haircutFlag = 0;
    }
}

class Complex
{
    ArrayList<Vertex> vertices;
    int core;

    public Complex()
    {
        vertices = new ArrayList<Vertex>();
        core = -1;
    }
}

class Mcode
{
    Graph g;
    Vertex vertices[];
    Vertex vw[];
    ArrayList<Complex> complexes;

    public Mcode(String s, double d, boolean postProcess, boolean haircut, boolean fluff, double d1)
    {
        g = new Graph(s);
        complexes = new ArrayList<Complex>();
        vertices = new Vertex[g.vertices.size()];
        vw = new Vertex[g.vertices.size()];
        vertexWeighting();
        qsortByWeight(vertices, 0, vertices.length - 1);
        for(int i = 0; i < vertices.length; i++)
        {
            Vertex vertex = vertices[i];
            Complex complex = new Complex();
            if(vertex.seen)
                continue;
            findComplex2(d, vertex, complex);
            if(complex.vertices.size() > 0)
                complexes.add(complex);
        }

        if(postProcess)
            postProcessing(haircut, fluff, d1);
    }
    
    //public void findClusterCore();

    public void postProcessing(boolean haircut, boolean fluff, double d)
    {
        for(int i = 0; i < complexes.size(); i++)
        {
            Complex complex = (Complex)complexes.get(i);
			if(complex.vertices.size() <2)
			{
				complexes.remove(i);
			}
	   		//if(complex.core < 2)
            //    complexes.removeAt(i);
        }

        if(haircut)		//check requirements to recursively analyse the graph
		{
			//haircut code here - eliminating vertices with degree 1
			for(int i=0; i<complexes.size(); i++)
			{
				Complex complex = (Complex)complexes.get(i);
				for(int j=0; j<complex.vertices.size(); j++)
				{
					Vertex u = complex.vertices.get(j);
					for(int k=0; k<complex.vertices.size(); k++)
					{
						if(u.haircutFlag > 1)	break;
						Vertex v = complex.vertices.get(k);
						if(g.im[u.index][v.index] == true)
						{
							v.haircutFlag++;
							u.haircutFlag++;
						}
					}
					if(u.haircutFlag<2)
					{
						complex.vertices.remove(j);
					}
				}
			}
		}
        
        if(fluff)
        {
            for(int i = 0; i < complexes.size(); i++)
            {
                Complex com = (Complex)complexes.get(i);

                for(int j = 0; j < vw.length; j++)
				{
                    if(com.vertices.contains(vw[j]))
                        vw[j].seen = true;
                    else
                        vw[j].seen = false;
				}
                for(int j = 0; j < com.vertices.size(); j++)
                {
                    ArrayList<Integer> nbr = g.findNeighbours(((Vertex)com.vertices.get(i)).index);
                    for(int k = 0; k < nbr.size(); k++)
                    {
                        if(!vw[((Integer)nbr.get(k)).intValue()].seen && vw[((Integer)nbr.get(k)).intValue()].weight > d)
                        {
                            com.vertices.add(vw[((Integer)nbr.get(k)).intValue()]);
                            vw[((Integer)nbr.get(k)).intValue()].seen = true;
                        }
					}
                }

            }

        }
    }

    public void printComplexes()
    {
        if(complexes.size() == 0)
        {
            System.out.println("NO COMPLEX FOUND ERROR");
            return;
        }
        for(int i = 0; i < complexes.size(); i++)
        {
            Complex complex = (Complex)complexes.get(i);
            System.out.print((new StringBuilder()).append("COMPLEX ").append(i).append(":\t").toString());
            for(int j = 0; j < complex.vertices.size(); j++)
                System.out.print((new StringBuilder()).append((String)g.vertices.get(((Vertex)complex.vertices.get(j)).index)).append("(").append(((Vertex)complex.vertices.get(j)).degree).append(",").append(((Vertex)complex.vertices.get(j)).coreNumber).append(")").append("  ").toString());

            System.out.println();
        }

    }

    public void findComplex2(double d, Vertex seed, Complex com)
    {
        if(seed.seen)
            return;
        com.vertices.add(seed);
        //com.core = seed.coreNumber <= com.core ? com.core : seed.coreNumber;
        seed.seen = true;
        ArrayList<Integer> nbr = g.findNeighbours(seed.index);
        for(int i = 0; i < nbr.size(); i++)
        {
            Vertex neighbour = vw[(nbr.get(i).intValue())];
            if(neighbour.weight > seed.weight * (1.0D - d))
                findComplex2(d, neighbour, com);
        }

    }

    public void findComplex(double d, Vertex vertex, Complex complex)
    {
        System.out.print((new StringBuilder()).append("\nPass with seed ").append(vertex.index).toString());
        if(vertex.seen)
        {
            System.out.print("\tSEEN");
            return;
        }
        System.out.print("\tSEEING");
        vertex.seen = true;
        ArrayList<Integer> nbr = g.findNeighbours(vertex.index);
        for(int i = 0; i < nbr.size(); i++)
        {
            Vertex vertex1 = vw[((Integer)nbr.get(i)).intValue()];
            if(vertex1.weight > vertex.weight * (1.0D - d))
            {
                complex.vertices.add(vertex1);
                System.out.print((new StringBuilder()).append("\tADDED the vertex ").append(vertex1.index).toString());
            } else
            {
                System.out.println((new StringBuilder()).append("Vertex ").append(vertex1.index).append(" skipped.").toString());
            }
            findComplex(d, vertex1, complex);
        }

    }

    public void vertexWeighting()
    {
        for(int i = 0; i < vertices.length; i++)
        {
            vertices[i] = new Vertex();
            vertices[i].index = i;
            vw[i] = vertices[i];
        }

        findCores();
        findDensity();
        for(int j = 0; j < vertices.length; j++)
            vertices[j].weight = vertices[j].coreDensity * vertices[j].coreNumber;

    }

    public static void qsortByDegree(Vertex avertex[], int i, int j)
    {
        if(j > i && i < j)
        {
            Vertex vertex = avertex[i];
            int k = i + 1;
            for(int l = i + 1; l <= j; l++)
                if(vertex.degree > avertex[l].degree)
                {
                    Vertex vertex1 = avertex[l];
                    avertex[l] = avertex[k];
                    avertex[k] = vertex1;
                    k++;
                }

            avertex[i] = avertex[k - 1];
            avertex[k - 1] = vertex;
            qsortByDegree(avertex, i, k - 2);
            qsortByDegree(avertex, k, j);
        }
    }

    public static void qsortByWeight(Vertex avertex[], int i, int j)
    {
        if(j > i && i < j)
        {
            Vertex vertex = avertex[i];
            int k = i + 1;
            for(int l = i + 1; l <= j; l++)
                if(vertex.weight < avertex[l].weight)
                {
                    Vertex vertex1 = avertex[l];
                    avertex[l] = avertex[k];
                    avertex[k] = vertex1;
                    k++;
                }

            avertex[i] = avertex[k - 1];
            avertex[k - 1] = vertex;
            qsortByWeight(avertex, i, k - 2);
            qsortByWeight(avertex, k, j);
        }
    }

    public void findCores()
    {
        for(int i = 0; i < vertices.length; i++)
        {
            for(int j = 0; j < g.order; j++)
	    {
                vertices[i].degree = g.im[vertices[i].index][j]?vertices[i].degree+1:vertices[i].degree;
            }
	}

        qsortByDegree(vertices, 0, vertices.length - 1);
        System.out.println("Sorted");
        for(int j = 0; j < vertices.length; j++)
        {
            vertices[j].coreNumber = vertices[j].degree;
            ArrayList<Integer> nbr = g.findNeighbours(vertices[j].index);
            for(int l = 0; l < nbr.size(); l++)
            {
                int i1 = ((Integer)nbr.get(l)).intValue();
                int j1 = 0;
                do
                {
                    if(j1 >= vertices.length)
                        break;
                    if(vertices[j1].index == i1)
                    {
                        if(vertices[j1].degree > vertices[j].degree)
                        {
                            vertices[j1].degree--;
                            int k = j1 - 1;
                            Vertex vertex;
                            for(vertex = vertices[j1]; k >= 0 && vertices[k].degree > vertex.degree; k--)
                                vertices[k + 1] = vertices[k];

                            vertices[k + 1] = vertex;
                        }
                        break;
                    }
                    j1++;
                } while(true);
                if(j1 == vertices.length)
                    System.out.println("NEIGHBOUR NOT FOUND ERROR");
            }

        }

    }

    public void findDensity()
    {
        for(int i = 0; i < vw.length; i++)
        {
            ArrayList<Integer> nbr = g.findNeighbours(i);
            nbr.add(new Integer(i));
            double edges = 0.0D;
            for(int j = 0; j < nbr.size();)
                if(vw[nbr.get(j).intValue()].coreNumber < vw[i].coreNumber)
                    nbr.remove(j);
                else
                    j++;

            for(int k = 0; k < nbr.size(); k++)
            {
                for(int l = k + 1; l < nbr.size(); l++)
                    if(g.im[k][l] == true)
                        edges++;

            }

            double totVertices = nbr.size();
            vw[i].coreDensity = edges / ((totVertices * (totVertices - 1.0D)) / 2D);
        }   
    }
}
