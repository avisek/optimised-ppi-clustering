import java.io.*;
class Test
{
  public static void main(String[] args)
  {
    if (args.length < 4)
    {
      System.out.println("USAGE: Test <input file name> <Clustering Density> <Operating Mode> <Fluff Threshold>");
      return;
    }
    double threshold = Double.parseDouble(args[1]);
    int i = Integer.parseInt(args[2]);
    boolean haricutFlag;
    boolean fluffFlag;
    double fluffThreshold;
    boolean postprocessFlag;
    switch (i)
    {
    case 0: 
    default: 
      haricutFlag = false;
      fluffFlag = false;
      fluffThreshold = 0.0D;
      postprocessFlag = false;
      break;
    case 1: 
      haricutFlag = false;
      fluffFlag = false;
      fluffThreshold = 0.0D;
      postprocessFlag = true;
      break;
    case 2: 
      haricutFlag = true;
      fluffFlag = false;
      fluffThreshold = 0.0D;
      postprocessFlag = true;
      break;
    case 3: 
      haricutFlag = false;
      fluffFlag = true;
      fluffThreshold = Double.parseDouble(args[3]);
      postprocessFlag = false;
      break;
    case 4: 
      haricutFlag = true;
      fluffFlag = true;
      fluffThreshold = Double.parseDouble(args[3]);
      postprocessFlag = true;
    }
    Mcode test = new Mcode(args[0], threshold, postprocessFlag, haricutFlag, fluffFlag, fluffThreshold);
    test.g.printMatrix();
    test.printComplexes();
  }
}
